package lotterychecker

import geb.Browser
import spock.lang.Specification

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel Reyes
 * Date: 31-Oct-16
 * Time: 10:10 AM
 */
class WebScrappingTest extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
        expect: "fix me"
        true == true
    }

    void "there is a loto"() {
        Browser.drive {
            go "file:///C:/Users/Enmanuel%20Reyes/Desktop/leidsa.html"
            Loto loto = new Loto()
            def balls = $('.loto-ball').allElements();
            for (int i = 1; i <= balls.size(); i++) {
                loto.setProperty("number" + i, balls.getAt(i - 1))
            }
            println loto


        }
        expect: "fix me"

        true == true
    }
}
